import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Appbar from './components/appbar'
import Index from './views/home'
import About from './views/about'


export default () => (
  <Router>
      <div>
        <Route path="*" exact component={Appbar} />
        <Route path="/" exact component={Index} />
        <Route path="/about/" component={About} />
      </div>
    </Router>
)

