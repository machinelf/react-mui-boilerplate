import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

export default class extends Component {
    state = {
        open : false
    }

    toggleDrawer = () => {
        this.setState({ open: !this.state.open })
    }
    render() {
        const { open } = this.state;

        return (
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton 
                            onClick={this.toggleDrawer} 
                            edge="start"  
                            color="inherit" aria-label="Menu"
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6">
                            App
                        </Typography>
                    </Toolbar>
                </AppBar>

                <Drawer open={open} onClose={this.toggleDrawer}>
                    <List style={{ width: '300px' }}>
                        <ListItem onClick={() => {
                            this.props.history.push('/');
                        }} button>
                            <ListItemText primary={'Home'} />
                        </ListItem>
                        <ListItem onClick={() => {
                            this.props.history.push('/about');
                        }} button>
                            <ListItemText primary={'About'} />
                        </ListItem>
                    </List>
                </Drawer>

            </div>
        )
    }
}
