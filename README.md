# React App Boilerplate

React app boilerplate for easily spinning up a client side web app. Written in ES6.

## To use
- Clone the repository
- `cd react-mui-boilerplate` and `npm i`
- Run `npm start`

## Frameworks/libraries
- [React](https://reactjs.org/)
- [React Router DOM](https://reacttraining.com/react-router/web/guides/quick-start)
- [Material UI](https://material-ui.com/)


## TODO
- Add Redux
- Add Auth flow
- Simulate API calls